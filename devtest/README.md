## Developer comments

The server is running in the root directory. I have created a new subfolder "devtest" in the root folder and installed Angular there.

The only thing what you need to do is to install the dependencies and configure the server url in src/app/app.components.ts file at the 16th line.

Mission has been accomplished. I hope that this is the solution what you guys wanted to see.