import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { OrderModule } from 'ngx-order-pipe';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
	HttpModule,
	OrderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
