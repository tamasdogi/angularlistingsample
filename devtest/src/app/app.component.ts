import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	
	orderBy: string = 'title';
	revstatus: boolean = false;
	details: boolean = false;
	
	private url = "http://192.168.0.101:8080/films";
	data: any = []; 
	activeMovie: any = [];
  
	constructor(private http: Http){
		this.getData();
		this.getFilms();
	}
  
	getData(){
		return this.http.get(this.url)
			.map((res: Response) => res.json())
	}

	getFilms(){
		this.getData().subscribe(data => {
			console.log(data);
			this.data = data;
		})
	}
	
	reOrder(order){
		
		if(this.revstatus==true){
			this.revstatus = false;
		}
		else{
			this.revstatus = true;
		}		
		this.orderBy = order;
	}
	
	openMoviePage(film_id){
		
		var dat = this.data;
		
		for (var i = 0; i < dat.length; i++) { 
			
			if(dat[i].id == film_id){
			
				this.details = true;
				console.log(dat[i]+" "+this.details);
				this.activeMovie = dat[i];
				
			}		
			
		}		
	}
	
  
}
